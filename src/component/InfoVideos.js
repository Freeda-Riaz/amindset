import React from 'react'
import { Container,Row ,Col } from "reactstrap";

export const InfoVideos = () => {
  return (
    <>
         <div className="p-5 mx-lg-5 mx-sm-0">
        <Row className="d-flex justify-content-center align-items-center ">
          <div className="text-center">
            <h2>Featured Articls</h2>
          </div>
          <div className="main-line" style={{ width: "50px" }}></div>
        </Row>
        <Row className="  my-3 d-flex justify-content-center align-items-center">
            <Col md="6" sm="12" lg="4" className=' d-flex justify-content-center align-items-center'>
            <div className="videoCard  pb-1">
              {" "}
             <div className="p-3 square border">
             <div className=" h-75 ">
                <img
                  src="images/video1.png"
                   className="w-100 h-100 text-white"
                ></img>
              </div>
              <div className="text-center "><b>Health Equity Animated :Race (Center for prevention MN)</b></div>
             </div>
            </div>
            </Col>
            <Col md="6" sm="12" lg="4" className=' d-flex justify-content-center align-items-center'>
            <div className="videoCard  pb-1">
              {" "}
             <div className="p-3 square border">
             <div className=" h-75 ">
                <img
                  src="images/video2.png"
                  className="w-100 h-100 text-white"
                ></img>
              </div>
              <div className="text-center "><b>Health Equity Animated :Race (Center for prevention MN)</b></div>
             </div>
            </div>
            </Col>
            <Col md="6" sm="12" lg="4" className=' d-flex justify-content-center align-items-center'>
            <div className=" videoCard  pb-1">
              {" "}
             <div className="p-3 square border">
             <div className=" h-75 ">
                <img
                  src="images/video1.png"
                  className="w-100 h-100 text-white"
                ></img>
              </div>
              <div className="text-center "> <b>Health Equity Animated :Race (Center for prevention MN)</b></div>
             </div>
            </div>
            </Col>
        </Row>
        <div>
        </div>
        </div>
    </>
  )
}
